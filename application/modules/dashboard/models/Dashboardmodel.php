<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboardModel extends MX_Controller {
	
	const STREAM = 'streams';
	const STREAM_DETAILS = 'streams_details';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function saveStream($data) {
		$this->db->insert(self::STREAM, $data);
		return $this->db->insert_id();
	}
	
	
}
/* End of file dashboardmodel.php */
/* Location: ./application/modules/dashboard/models/dashboardmodel.php */
