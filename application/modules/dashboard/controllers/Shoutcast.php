<?php

class Shoutcast extends MX_Controller
{
	private $shoutcastServer = '';
	private $shoutcastServerFolder = '';
	private $shoutcastServersFolder = '';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('dashboard/dashboardmodel', 'dashboard');
		$this->shoutcastServer = FCPATH . 'bin/sc_serv';
		$this->shoutcastServersFolder = FCPATH . 'bin/server/';
		$this->shoutcastServerFolder = FCPATH . 'bin/';
	}
	
	public function index() 
	{
		echo $this->shoutcastServer;
		$this->_createServer();
	}
	
	public function startServer()
	{
		system($this->shoutcastServer . " " . $this->shoutcastServersFolder . "1.conf > /dev/null 2>&1 & echo \$!");
	}
	
	private function _createServer() 
	{
		$config = [
				'adminpassword'			=> 'Admin123451'
		        ,'maxuser'				=> 100
				,'listenertime'			=> 0
				,'autodumusers'			=> 0
		        ,'password'				=> 123456
		        ,'portbase'				=> 8000
				,'publicserver'			=> 'always'
				,'logfile'				=> $this->shoutcastServerFolder . 'logs/log_1.log'
				,'w3clog'				=> $this->shoutcastServerFolder . 'logs/w3c_1.log'
				,'banfile'				=> $this->shoutcastServerFolder . 'control/serv_1.ban'
				,'savebanlistonexit'	=> 0
				,'ripfile'				=> $this->shoutcastServerFolder . 'control/serv_1.rip'
				
		        
		];
		$idServer = $this->dashboard->saveStream(['stream_name' => 'demo ' . rand(0,9999)]);
		echo $idServer;
		//$this->_createServerFile(1, $config);
	}
	
	private function _createServerFile($server, $config)
	{
		$newFileServerConfig = $this->shoutcastServersFolder . $server . '.conf';
		$data = ";\n"
        . "; JyC Shoutcast server Configuration file\n"
        . "; Powered by JyConsultores\n"
        . "; Website: http://jyconsultores.com\n"
        . "; Email: Aisenhaim@gmail.com\n"
        . "; Created by: Jose Luis Huamani Gonzales\n"
        . ";\n";
		if(is_array($config))
		{
			foreach($config as $k => $v)
			{
				$data .= $k . "=" . $v ."\n";
			}
		}
		if (file_put_contents($newFileServerConfig, $data) !== false) 
		{
			file_put_contents($this->shoutcastServerFolder . 'logs/log_' . $server . '.log', '');
			file_put_contents($this->shoutcastServerFolder . 'logs/w3c_' . $server . '.log', '');
			file_put_contents($this->shoutcastServerFolder . 'control/serv_' . $server . '.ban', '');
			file_put_contents($this->shoutcastServerFolder . 'control/serv_' . $server . '.rip', '');
			echo "File created (" . basename($newFileServerConfig) . ")";
		} else {
			echo "Cannot create file (" . basename($newFileServerConfig) . ")";
		}
	}
}